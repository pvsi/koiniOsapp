//
//  userProfile.swift
//  koinapps
//
//  Created by Dzakwan ilham yahya on 06/03/18.
//  Copyright © 2018 Dzakwan ilham yahya. All rights reserved.
//

import SwiftyJSON

class userProfile {
    
    private init(){}
    
    static let profile = userProfile()

    var store_phone: String = ""
    var name: String = ""
    var email: String = ""
    var saldo: String = ""
    var poin: String = ""
    var status: String = ""
    var level: String = ""
    var password:String = ""
    var lastActiveScreen = ""
    var lastActiveRequest = ""
    
    static func parseUserNow(parses:JSON) -> Void {
        userProfile.profile.name = parses["name"].stringValue
        userProfile.profile.email = parses["email"].stringValue
        userProfile.profile.saldo = parses["saldo"].stringValue
        userProfile.profile.poin = parses["poin"].stringValue
        userProfile.profile.status = parses["status"].stringValue
        userProfile.profile.level = parses["level"].stringValue
    }
}


