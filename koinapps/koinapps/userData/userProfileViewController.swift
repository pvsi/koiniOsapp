//
//  userProfileViewController.swift
//  koinapps
//
//  Created by Dzakwan ilham yahya on 23/03/18.
//  Copyright © 2018 Dzakwan ilham yahya. All rights reserved.
//

import UIKit

class userProfileViewController: UIViewController {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    override func viewDidLoad() {
        
        lblName.text = userProfile.profile.name
        lblEmail.text = userProfile.profile.email
        lblStatus.text = userProfile.profile.status
        
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
