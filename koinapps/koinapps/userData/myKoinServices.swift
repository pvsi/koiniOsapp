//
//  myServices.swift
//  koinapps
//
//  Created by Dzakwan ilham yahya on 02/03/18.
//  Copyright © 2018 Dzakwan ilham yahya. All rights reserved.
//

import Foundation

class listServices: Decodable {
    var services:[myKoinServices] = []
}

struct myKoinServices:Decodable {
    let command: String
    let name: String
    let id: String
}

