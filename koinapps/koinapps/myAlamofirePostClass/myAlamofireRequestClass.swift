//
//  myAlamofirePostClass.swift
//  koinapps
//
//  Created by Dzakwan ilham yahya on 21/03/18.
//  Copyright © 2018 Dzakwan ilham yahya. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol AlamofireRequesttListener {
    func successResponse(response: DataResponse<Any>)
    func failureResponse(response: DataResponse<Any>)
}
let baseurl:String = "http://stage.koin.id:8100/KoinDev/public/api/koin"
class myAlamofireClass{

    static func showNetworkIndicator(){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    static func hideNetworkIndicator(){
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    static func callAlamofire(urlapi: String,method: HTTPMethod, param: Parameters?, listener: AlamofireRequesttListener){
        showNetworkIndicator()
        Alamofire.request(urlapi, method: method, parameters: param, encoding: URLEncoding.httpBody).responseJSON {(response) in
            switch response.result {
            case .success:
                listener.successResponse(response: response)
                hideNetworkIndicator()
            case .failure:
                listener.failureResponse(response: response)
                hideNetworkIndicator()
            }
        }
    }

    static func loginRequest(param:Parameters,listener: AlamofireRequesttListener) {
        callAlamofire(urlapi: baseurl, method: .post, param: param, listener: listener)
    }
    
    static func registerRequest(param:Parameters,listener: AlamofireRequesttListener) {
        callAlamofire(urlapi: baseurl, method: .post, param: param, listener: listener)
    }
    
    static func verificationRequest(param:Parameters,listener:AlamofireRequesttListener) {
        callAlamofire(urlapi: baseurl, method: .post, param: param, listener: listener)
    }

    static func resendverification(param:Parameters,listener: AlamofireRequesttListener){
        callAlamofire(urlapi: baseurl, method: .post, param: param, listener: listener)
    }
}
