//
//  mainViewController.swift
//  koinapps
//
//  Created by Dzakwan ilham yahya on 02/03/18.
//  Copyright © 2018 Dzakwan ilham yahya. All rights reserved.
//

import UIKit

class mainViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    
    @IBOutlet weak var collectionnView: UICollectionView!
    var arrServices = listServices()
    var selected:Int = 0
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSaldo: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrServices.services.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tableCell", for: indexPath) as! servicesCollectionViewCell
        let row = indexPath.row
        cell.imgService.image = UIImage(named: "gmbr"+arrServices.services[row].id)
        
        return cell
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0{
        performSegue(withIdentifier: "seguetoService1", sender: indexPath)
        }else{
        performSegue(withIdentifier: "seguetoService2", sender: indexPath)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !userProfile.profile.name.isEmpty {
            lblName.text = userProfile.profile.name
        }
        lblSaldo.text = "Rp. \(userProfile.profile.saldo),-"
        
        //collection view set data
        dummydata();
        collectionnView.dataSource = self
        collectionnView.delegate = self
        
        //collection view set layout 4-4
        setCollectionViewLayout()
        
        //make imgProfile circle
        makeCircleAvatar()
        
    }
    
    func makeCircleAvatar() -> Void {
        imgProfile.layer.borderWidth = 1
        imgProfile.layer.masksToBounds = false
        imgProfile.layer.borderColor = UIColor.black.cgColor
        imgProfile.layer.cornerRadius = imgProfile.frame.height/2
        imgProfile.clipsToBounds = true
    }
    
    func setCollectionViewLayout() -> Void {
        let layout = UICollectionViewFlowLayout();
        let itemsize = UIScreen.main.bounds.width/4-5
        layout.sectionInset = UIEdgeInsets(top: 20, left: 2, bottom: 10, right: 2)
        layout.itemSize = CGSize(width: itemsize, height: itemsize)
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 5
        collectionnView.collectionViewLayout = layout;
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func dummydata() -> Void{
        let dummyservice1 = myKoinServices(command: "command 1", name: "dum1", id: "1")
        let dummyservice2 = myKoinServices(command: "command 2", name: "dum2", id: "2")
        let dummyservice3 = myKoinServices(command: "command 3", name: "dum3", id: "3")
        
        arrServices.services.append(dummyservice1)
        arrServices.services.append(dummyservice2)
        arrServices.services.append(dummyservice3)
        arrServices.services.append(dummyservice2)
        arrServices.services.append(dummyservice1)
        arrServices.services.append(dummyservice2)
        arrServices.services.append(dummyservice3)
    }

}

