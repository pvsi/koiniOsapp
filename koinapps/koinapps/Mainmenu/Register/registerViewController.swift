//
//  RegisterViewController.swift
//  koinapps
//
//  Created by Dzakwan ilham yahya on 01/03/18.
//  Copyright © 2018 Dzakwan ilham yahya. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CryptoSwift

class registerViewController: UIViewController,AlamofireRequesttListener {
    
    func successResponse(response: DataResponse<Any>) {
        let abc = response.result.value!
        let res = JSON(abc)
        let flag = res["response"].stringValue != "0"
        print("ini respionse dari register :")
        print(res)
        if  flag{
            buildAlert(title: "REGISTER GAGAL", message: res["message"].stringValue, completion: nil, handler: nil)
        } else {
            print(userProfile.profile.store_phone + "INI STOREpOhn" + userProfile.profile.password + "inni paszzord")
            buildAlert(title: "REGISTER BERHASIL", message: res["message"].stringValue, completion: nil, handler: { action in
                userProfile.profile.lastActiveScreen = "RegisterPage"
                self.performSegue(withIdentifier: "seguetoWait", sender: self)
            })
        }
    }
    
    func failureResponse(response: DataResponse<Any>) {
        print(response)
        let errs = response.result.error!
        print("Error : \(errs)!")
        print("gagal server")
    }
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhonenumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtPassword2: UITextField!
    
    @IBOutlet weak var texted: UITextView!
    @IBAction func btnRegister(_ sender: Any) {
        if txtName.text!.isEmpty || txtPhonenumber.text!.isEmpty || txtEmail.text!.isEmpty || txtPassword.text!.isEmpty {
            buildAlert(title: "Register Failed", message: "Login failed, please input the missing fields", completion: nil, handler: nil)
        }else if !isValidStorePhone(testStr: txtPhonenumber.text!){
            buildAlert(title: "Register Failed", message: "Invalid phone number format please use +62xx or 08xx instead", completion: nil, handler: nil)
        }else if !isValidEmail(testStr: txtEmail.text!) {
            buildAlert(title: "Register Failed", message: "Invalid email format", completion: nil, handler: nil)
        }else if txtPassword.text!.count < 6{
            buildAlert(title: "Register Failed", message: "Password must atleast 6 characters long", completion: nil, handler: nil)
        }else if txtPassword.text! != txtPassword2.text!{
            buildAlert(title: "Register Failed", message: "Password didnt match !", completion: nil, handler: nil)
        }else{
            userProfile.profile.store_phone = txtPhonenumber.text!
            userProfile.profile.password = txtPassword.text!
            myAlamofireClass.registerRequest(param: generateparameters(Username: txtName.text!, User_phone: txtPhonenumber.text!, Email: txtEmail.text!, ktsnd: txtPassword.text!), listener: self)
        }
    }
    @IBAction func btnBackToLogin(_ sender: Any) {
        performSegue(withIdentifier: "unwindtoLogin", sender: self)
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9]+\\.[A-Za-z]+$" //Email format
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidStorePhone(testStr:String) -> Bool {
        let emailRegEx = "^(^\\+62\\s?|^0)(\\d{3,4}-?){2}\\d{3,4}$"         //Start with (62|08) with length {10,13}
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func generateparameters(Username:String,User_phone:String,Email:String,ktsnd: String) -> [String : String] {
        var res = MyFunctions.generateParameters(command: "register", password: "d4t4c3ll")//parameter utama
        res["email"] = Email                                                                 //parameter tambahan
        res["name"] = Username
        res["password"] = ktsnd.md5()
        print(res)
        return res
    }
    
    func buildAlert(title: String, message: String, completion: (() -> Void)?, handler: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: handler)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: completion)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
}
