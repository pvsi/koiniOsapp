//
//  waitViewController.swift
//  koinapps
//
//  Created by Dzakwan ilham yahya on 20/03/18.
//  Copyright © 2018 Dzakwan ilham yahya. All rights reserved.
//
import Alamofire
import CryptoSwift
import KWVerificationCodeView
import RxCocoa
import RxSwift
import SwiftyJSON
import UIKit

class waitViewController: UIViewController,AlamofireRequesttListener,KWVerificationCodeViewDelegate {
    var vm = waitViewModel()
    
    func didChangeVerificationCode() {
        if txtVerificationCode.hasValidCode(){
            myAlamofireClass.verificationRequest(param: parameterVerification(code: txtVerificationCode.getVerificationCode(),user_phone:userProfile.profile.store_phone), listener: self)
        }
    }
    
    func successResponse(response: DataResponse<Any>) {
        let res = JSON(response.result.value!)
        let flag = res["response"].stringValue != "0"
        print("\(res) ========================")
        if res["response"] == "1" {
            buildAlert(title: "Berhasil Resend Code", message: res["message"].stringValue, completion: nil, handler: nil)
        }else{
            if flag {
                buildAlert(title: "Kode salah", message: res["message"].stringValue, completion: nil, handler: nil)
            }else if userProfile.profile.lastActiveScreen == "RegisterPage"{
                buildAlert(title: "Berhasil", message: res["message"].stringValue, completion: nil, handler: {action in
                        userProfile.profile.lastActiveScreen = "ConfirmationPage"
                        self.performSegue(withIdentifier: "seguetoLogin", sender: self)
                    }
                )
            }else if userProfile.profile.lastActiveScreen == "LoginPage"{
                buildAlert(title: "Berhasil", message: res["message"].stringValue, completion: nil, handler: {action in
                        userProfile.parseUserNow(parses: res)
                        userProfile.profile.lastActiveScreen = "ConfirmationPage"
                        self.performSegue(withIdentifier: "seguetoMainMenu", sender: self)
                    }
                )
            }
        }
    }
    
    func failureResponse(response: DataResponse<Any>) {
        print(response)
        let errs = response.result.error!
        print("Error : \(errs)!")
        print("gagal server")
    }
    
    @IBOutlet weak var txtUserPhone: UILabel!
    @IBOutlet weak var txtVerificationCode: KWVerificationCodeView!
    
    @IBAction func btnreSendVerificationCode(_ sender: Any) {
        myAlamofireClass.resendverification(param: parameterReSendVerification(user_phone:userProfile.profile.store_phone), listener: self)
    }
    
    func parameterVerification(code:String ,user_phone:String) -> [String : String] {
        var res = MyFunctions.generateParameters(command: "verificationcode", password: userProfile.profile.password)                       //parameter utama
        res["code"] = code                                                                                                                  //parameter tambahan
        res["lastActiveScreen"] = userProfile.profile.lastActiveScreen
        print(res)
        return res
    }
    func parameterReSendVerification(user_phone:String) -> [String : String] {
        let res = MyFunctions.generateParameters(command: "resendcode", password: userProfile.profile.password)                             //parameter utama
        return res
    }
    
    func buildAlert(title: String, message: String, completion: (() -> Void)?, handler: ((UIAlertAction) -> Void)?) {                       //alert maker
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: handler)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: completion)
    }
    
    override func viewDidLoad() {
        if userProfile.profile.store_phone.isEmpty {
            txtUserPhone.text = userProfile.profile.store_phone
        }
        txtVerificationCode.delegate = self
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
