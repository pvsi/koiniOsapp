//
//  waitViewModel.swift
//  koinapps
//
//  Created by Dzakwan ilham yahya on 02/04/18.
//  Copyright © 2018 Dzakwan ilham yahya. All rights reserved.
//

import Foundation
import RxSwift

struct waitViewModel {
    

    var responseCode = Variable<String>("")
    var store_phone = Variable<String>("")
    
    
    var isValid:Observable<Bool>{
        return Observable.combineLatest(responseCode.asObservable(), store_phone.asObservable())
        {code,phone in
            validate(teststr: code,teststr2: phone)
        }
    }
}

func validate(teststr:String,teststr2:String) -> Bool {
    
    let params = codeParameter(code: teststr, user_phone: teststr2)
    
    if teststr.isEmpty && teststr2.isEmpty{
        return false
    }else if teststr.count < 5 {
        return false
    }
    else{
        //myAlamofireClass.secretCodeRequest(param: params, listener: /*disini*/)
        return true
    }
}
func codeParameter (code:String,user_phone:String )->[String:String]{
    return [code:user_phone]
}


