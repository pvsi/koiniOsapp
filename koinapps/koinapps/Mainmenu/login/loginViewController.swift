//  ViewController.swift
//  koinapps
//
//  Created by Dzakwan ilham yahya on 01/03/18.
//  Copyright © 2018 Dzakwan ilham yahya. All rights reserved.
//
import Alamofire
import ARSLineProgress
import CryptoSwift
import SwiftyJSON
import UIKit
import RxCocoa
import RxSwift
class loginViewController: UIViewController,AlamofireRequesttListener {
    
    //lazy var progress = progressDialog(delegate: self )
    
    func successResponse(response: DataResponse<Any>) {
        let res = JSON(response.result.value!)
        let flag = res["response"].stringValue != "0"
        userProfile.profile.lastActiveScreen = "LoginPage"
        print(res)
        if flag{
            if res["response"].stringValue == "E7"{
                buildAlert(title: "Akun gagal", message: res["message"].stringValue, completion: nil, handler: {action in
                    self.performSegue(withIdentifier: "seguetoWait", sender: self)
                })
            }
            buildAlert(title: "LOGIN GAGAL", message: res["message"].stringValue, completion: nil, handler: nil)
        }else{
            userProfile.parseUserNow(parses: res)
            buildAlert(title: "LOGIN BERHASIL", message: "selamat datang \(res["name"].stringValue)", completion: nil, handler: { action in
                self.performSegue(withIdentifier: "seguetoMainMenu", sender: self)
            })
        }
    }
    func failureResponse(response: DataResponse<Any>) {
        print(response)
        let errs = response.result.error!
        print("Error : \(errs)!")
        print("gagal server")
    }
    
    //all Outlet
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var phonenumberTv: UITextField!
    @IBOutlet weak var passwordTv: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    var iconClick : Bool!

    //all Action
    @IBAction func btnLogin(_ sender: Any) {
        if phonenumberTv.text!.isEmpty || passwordTv.text!.isEmpty {
            buildAlert(title: "Login Failed", message: "Please fill the missing fields", completion: nil, handler: nil)
        }else if !isValidStorePhone(testStr: phonenumberTv.text!){
            buildAlert(title: "Login Failed", message: "Invalid phone number format please use +62xx or 08xx instead", completion: nil, handler: nil)
        }else{
            userProfile.profile.store_phone = phonenumberTv.text!
            userProfile.profile.password = passwordTv.text!
            myAlamofireClass.loginRequest(param: generateParameters(User_phone: userProfile.profile.store_phone, ktsnd: userProfile.profile.password), listener: self)
        }
    }
    
    @IBAction func btnRegister(_ sender: Any) {
        performSegue(withIdentifier: "seguetoRegister", sender: self)
    }
    @IBAction func btnForgetPassword(_ sender: Any) {
        performSegue(withIdentifier: "seguetoForget", sender: self)
    }
    @IBAction func btnHidePassword(_ sender: Any) {
        if(iconClick == true) {
            passwordTv.isSecureTextEntry = false
            iconClick = false
        } else {
            passwordTv.isSecureTextEntry = true
            iconClick = true
        }
    }
    
    //unwind
    @IBAction func unwindtoLogin(_ sender:UIStoryboardSegue){
    }
    
    func isValidStorePhone(testStr:String) -> Bool {
        let emailRegEx = "^(^\\+62\\s?|^0)(\\d{3,4}-?){2}\\d{3,4}$"         //Start with (62|08) with length {10,13}
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func generateParameters(User_phone:String,ktsnd: String) -> [String : String] {
        var res = MyFunctions.generateParameters(command: "login", password: userProfile.profile.password)                     //parameter utama
        res["lat"] = "123"
        res["long"] = "123"
        print(res)
        return res
    }
    
    func buildAlert(title: String, message: String, completion: (() -> Void)?, handler: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: handler)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: completion)
    }
    
    override func viewDidLoad() {
        iconClick = true
        passwordTv.isSecureTextEntry = true
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

