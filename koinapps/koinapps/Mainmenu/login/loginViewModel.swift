//
//  LoginViewModel.swift
//  koinapps
//
//  Created by Dzakwan ilham yahya on 19/03/18.
//  Copyright © 2018 Dzakwan ilham yahya. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct loginViewModel {
    
    var storephone = Variable<String>("")
    var password = Variable<String>("")
    
    var isValid: Observable<Bool>{
        return Observable.combineLatest(storephone.asObservable(),password.asObservable()){phone,password in
         isValidStorePhone(testStr: phone) && isValidPassword(testStr: password)
        }
    }
}

func isValidStorePhone(testStr:String) -> Bool {
    let emailRegEx = "^(^\\+62\\s?|^0)(\\d{3,4}-?){2}\\d{3,4}$"         //Start with (62|08) with length {10,13}
    let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}

func isValidPassword(testStr:String) -> Bool {
    let passwordRegEx = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$"      //Minimum six characters, at least one letter and one number:
    let emailTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
    return emailTest.evaluate(with: testStr)
}
