//
//  welcomeViewController.swift
//  koinapps
//
//  Created by Dzakwan ilham yahya on 10/04/18.
//  Copyright © 2018 Dzakwan ilham yahya. All rights reserved.
//

import UIKit
import paper_onboarding

class welcomeViewController: UIViewController,PaperOnboardingDataSource {
    
    var itemArr:[OnboardingItemInfo] = []
    
    func onboardingItemsCount() -> Int {
        return itemArr.count
    }
    
    func onboardingItem(at index: Int) -> OnboardingItemInfo {
        
        return itemArr[index]
    }
    
    
    @IBOutlet weak var onboardingView: onBoardingView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datadummy()
        onboardingView.dataSource = self
    }
    
    func datadummy() -> Void {
        
        let bgcolor = UIColor(red: 237/255, green: 235/255, blue: 238/255, alpha: 1)
        let bigfont = UIFont(name: "AvenirNext-Bold", size: 24)
        let smallFont = UIFont(name: "AvenirNext-Regular", size: 18)!

        let img1 = UIImage(named: "k-dotbig")
        let title1 = "Kendrick Lamar"
        let desc1 = "Kendrick Lamar Duckworth (born June 17, 1987) is an American rapper and songwriter. Raised in Compton, California, Lamar embarked on his musical career as a teenager under the stage name K-Dot, releasing a mixtape that garnered local attention and led to his signing with indie record label Top Dawg Entertainment (TDE)"
        let pageicon1 = UIImage(named: "k-dot")
        
        let img2 = UIImage(named: "logicbig")
        let title2 = "Logic"
        let desc2 = "Sir Robert Bryson Hall II (born January 22, 1990), known by his stage name Logic, is an American rapper, singer, songwriter, and record producer. Raised in Gaithersburg, Maryland, Logic developed an interest in music as a teenager, and ventured into a musical career in early 2009 releasing Logic: The Mixtape under the name Psychological, and releasing a mixtape titled Young, Broke & Infamous in 2010."
        let pageicon2 = UIImage(named: "logic")
        
        let img3 = UIImage(named: "postmalonebig")
        let title3 = "Post-malone"
        let desc3 = """
            Austin Richard Post (born July 4, 1995), known professionally as Post Malone, is an American singer, rapper, songwriter, and record producer.He first gained major recognition in August 2015, after the release of his debut single "White Iverson" before landing a record deal with Republic Records in the same month.
            """
        let pageicon3    = UIImage(named: "postmalone")
        
        
        let tutorial1 = OnboardingItemInfo(informationImage: img1!, title: title1, description: desc1, pageIcon: pageicon1!, color: bgcolor, titleColor: UIColor.black, descriptionColor: UIColor.black, titleFont: bigfont!, descriptionFont: smallFont)
        
        let tutorial2 = OnboardingItemInfo(informationImage: img2!, title: title2, description: desc2, pageIcon: pageicon2!, color: bgcolor, titleColor: UIColor.black, descriptionColor: UIColor.black, titleFont: bigfont!, descriptionFont: smallFont)
        
        let tutorial3 = OnboardingItemInfo(informationImage: img3!, title: title3, description: desc3, pageIcon: pageicon3!, color: bgcolor, titleColor: UIColor.black, descriptionColor: UIColor.black, titleFont: bigfont!, descriptionFont: smallFont)
        
        
        itemArr.append(tutorial1)
        itemArr.append(tutorial2)
        itemArr.append(tutorial3)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
